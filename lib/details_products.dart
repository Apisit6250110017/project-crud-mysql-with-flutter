import 'package:flutter/material.dart';
import 'package:project_demo_2/edit_products.dart';
import 'package:project_demo_2/models/products.dart';
import 'package:http/http.dart' as http;

class Details extends StatefulWidget {
  final Products products;

  Details({required this.products});

  @override
  _DetailsState createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  void deleteProducts(context) async {
    await http.post(
      Uri.parse("http://10.0.2.2/android_api/delete_product.php"),
      body: {
        'pid': widget.products.pid.toString(),
      },
    );
    // Navigator.pop(context);
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  void confirmDelete(context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25.0),
          ),
          content: Text('Are you sure you want to delete this?',style: TextStyle(fontWeight: FontWeight.bold),),
          actions: <Widget>[
            ElevatedButton.icon(
              label: Text('No'),
              icon: Icon(Icons.cancel),
              style: ElevatedButton.styleFrom(
                primary: Colors.white,
                onPrimary: Colors.orangeAccent,
              ),
              onPressed: () => Navigator.of(context).pop(),
            ),
            ElevatedButton.icon(
              label: Text('Yes'),
              icon: Icon(Icons.check_circle),
              style: ElevatedButton.styleFrom(
                primary: Colors.deepOrange,
                onPrimary: Colors.white,
              ),
              onPressed: () => deleteProducts(context),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepOrange,
        title: Text('Products Details'),

      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          width: 400,
          height: 200,
          // height: 270.0,
          // padding: const EdgeInsets.all(35),

          child: Card(
            elevation: 40,
            color: Colors.deepOrange[45],
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 10, left: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Row(
                    children: [
                      Text(
                        "Name Products : ${widget.products.name}",
                        style: TextStyle(
                            fontSize: 25, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        "Price : ${widget.products.price}",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        "description : ${widget.products.description}",
                        style: TextStyle(fontSize: 15),
                      ),
                    ],
                  ),
                  ButtonBar(
                    children: [
                      FlatButton(
                          onPressed: () => Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) =>
                                      Edit(products: widget.products),
                                ),
                              ),
                          child: Text(
                            'Edit ',
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Colors.greenAccent),
                          ),
                      ),
                      FlatButton(
                          onPressed: () => confirmDelete(context),

                          child: Text(
                            'Delete',
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Colors.red),
                          )
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),

    );
  }
}
