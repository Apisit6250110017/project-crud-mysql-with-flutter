import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ProductCreate extends StatefulWidget {
  const ProductCreate({Key? key}) : super(key: key);

  @override
  _ProductCreateState createState() => _ProductCreateState();
}

class _ProductCreateState extends State<ProductCreate> {
  TextEditingController nameController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  // Http post request to create new data
  Future _createProduct() async {
    return await http.post(
      Uri.parse("http://10.0.2.2/android_api/insert_product.php"),
      body: {
        "name": nameController.text,
        "price": priceController.text,
        "description": descriptionController.text,
      },
    );
  }

  void _onConfirm(context) async {
    await _createProduct();

    // Remove all existing routes until the Home.dart, then rebuild Home.
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Create Products"),
        backgroundColor: Colors.deepOrange,
        shadowColor: Colors.black,
        elevation: 10,
      ),


      floatingActionButton: Padding(
        padding: const EdgeInsets.only(right: 125),
        child: FloatingActionButton.extended(

          onPressed: () {
            _onConfirm(context);
          },
          label: Text('Save',style: TextStyle(
            color: Colors.deepOrange,
          ),),
          icon: Icon(Icons.save,color: Colors.deepOrange,),
          backgroundColor: Colors.white,
        ),
      ),

      body: Container(
        height: 400,
        width: 600,
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(40.0)),

            elevation: 40,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
              children: <Widget>[
                Container(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                  controller: nameController,
                  decoration: InputDecoration(
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.deepOrangeAccent)
                      ),
                      labelText: " Name Products:",
                      labelStyle: TextStyle(color: Colors.deepOrange),
                      hintText: "Enter  Name Products  ",
                  ),
                ),
                    )),
                Container(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(

                  controller: priceController,
                  decoration: InputDecoration(
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.deepOrangeAccent)
                      ),
                      labelText: "price Products:",
                      labelStyle: TextStyle(color: Colors.deepOrange),
                      hintText: "Enter price Products",
                  ),
                ),
                    )),
                Container(

                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(

                  controller: descriptionController,
                  decoration: InputDecoration(
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.deepOrangeAccent)
                      ),
                      labelText: "description Products:",
                      labelStyle: TextStyle(color: Colors.deepOrange),
                      hintText: "Enter description Products",
                  ),
                ),
                    )),
              ],
          ),
            ),
      ),
        ),
      )
    );
  }
}
