import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:project_demo_2/about/about_apisit.dart';
import 'package:project_demo_2/about/about_mantita.dart';
import 'package:project_demo_2/models/products.dart';
import 'package:project_demo_2/product_create.dart';
import 'package:http/http.dart' as http;

import 'details_products.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late Future<List<Products>> products;

  @override
  void initState() {
    super.initState();
    products = getProductsList();
  }

  Future<List<Products>> getProductsList() async {
    String url = "http://10.0.2.2/android_api/list_products.php";
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return productsFromJson(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load Products');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepOrange,
        title: Text("list Products"),
        shadowColor: Colors.black,
        elevation: 10,
      ),
      body: Center(
        child: FutureBuilder<List<Products>>(
          future: products,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            // By default, show a loading spinner.
            if (!snapshot.hasData) return CircularProgressIndicator();
            // Render student lists
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                var data = snapshot.data[index];
                return Padding(
                  padding: const EdgeInsets.all(7.0),
                  child: Card(
                    elevation: 10,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                    child: ListTile(
                      leading: Icon(
                        Icons.pageview,
                        size: 30,
                        color: Colors.deepOrangeAccent,
                      ),
                      trailing: Icon(
                        Icons.arrow_forward_ios,
                        color: Colors.deepOrangeAccent,
                      ),
                      title: Text(
                        data.name,
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      subtitle: Text(
                        data.price,
                      ),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Details(products: data)),
                        );
                      },
                    ),
                  ),
                );
              },
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(

        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ProductCreate()),
          );
        },
        label: Text('Add',style: TextStyle(
          color: Colors.deepOrange,
        ),),
        icon: const Icon(
          Icons.add,
          color: Colors.deepOrange,
        ),
        backgroundColor: Colors.white,
      ),
      drawer: Drawer(
     child: ListView(padding: const EdgeInsets.all(0.0), children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: const Text(
              'Menu About',
              style: TextStyle(fontSize: 25),
            ),
            accountEmail: const Text('About up'),
          ),
          ListTile(
              title: const Text('Apisit Madhadam'),
              leading: const Icon(Icons.account_circle_sharp),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const AboutApisit()));
              }),
          ListTile(
              title: const Text('Mantita Rabeadee'),
              leading: const Icon(Icons.account_circle_sharp),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const AboutMantita()));
              }),
          const Divider(),
          ListTile(
              title: const Text('Close'),
              leading: const Icon(Icons.close),
              onTap: () {
                Navigator.of(context).pop();
              }),
        ]),
      ),


    );
  }
}
